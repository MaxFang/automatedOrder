#!/usr/bin/env python
#encoding:utf-8

import logging
import os
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

#写log用

logfilename = "single.1688.log"
if not os.path.exists("log"):
	os.mkdir("log")
logfile = "log/" + logfilename

################## 创建日记  #######################
logger = logging.getLogger()
logger.setLevel(logging.INFO)
fh = logging.FileHandler(logfile)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s:  %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)


#alllogstrs = ""
def logInfo(logstrs):
	alllogstrs = ""
	logger.info(logstrs)
	#alllogstrs += logstrs
